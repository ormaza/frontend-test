# Teste para candidatos à vaga de desenvolvedor Front-end da Interativa Digital.

Crie um fork deste repositório para salvar e nos dar acesso ao seu trabalho

### Instruções
1. Crie um fork deste repositório para salvar e nos dar acesso ao seu trabalho;
2. Criar o HTML/CSS com Bootstrap 4;
3. Nós queremos avaliar o seu nível de conhecimento em design responsivo usando HTML, CSS e JS;
4. Ter uma boa orientação de commits.

#### O que é permitido:
- Uso de Task runners/build tools como Webpack, Gulp, Grunt e afins;
- Pré-processadores CSS: Sass, LESS, Stylus.
### Material 
Disponibilizamos o PSD de referência para a execução deste desafio. Você pode encontralo-lo no diretorio PSD.

### O objetivo desse teste é avaliar:
- Organização;
- Semântica;
- Funcionamento e usabilidade;
- Uso das features das linguagens (HTML, CSS e JS);
- Performance do código;
- README.md bem documentado com procedimentos para executar o projeto.

### Prazo
3 dias após o recebimento do email.
